package front;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class GUI {
	public JFrame frame = new JFrame();
	
	private JPanel pane;
	private JLabel p1, p2, res;
	
	private JTextField P1, P2, result;
	private JButton add, subtract, multiply, divide, differentiate, integrate;
	Controller control;
	
	public GUI()
	{
		initialize();		
	}
	
	private void initialize()
	{
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(650, 190);
		frame.setResizable(false);
		pane = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();	
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridheight = 1;
		c.gridwidth = 1;
		
		c.weightx = 20;
		c.weighty = 10.f;
		frame.add(pane);
		
		P1 = new JTextField();
		P2 = new JTextField();
		result = new JTextField();
		
		c.gridx = 0; c.gridy = 1;
		pane.add(P1, c);
		
		c.gridx = 0; c.gridy = 3;
		pane.add(P2, c);
		
		c.gridx = 0; c.gridy = 20;
		result.setEditable(false); //Can't edit the operation result
		pane.add(result, c);
		
		
		c.weightx = 0.25f;
		c.weighty = 0.10f;
		
		c.gridx = 0; c.gridy = 0;
		p1 = new JLabel("P1(X)");
		pane.add(p1, c);
		
		c.gridx = 0; c.gridy = 2;
		p2 = new JLabel("P2(X)");
		pane.add(p2, c);
		
		c.gridx = 0; c.gridy = 5;
		res = new JLabel("Result");
		pane.add(res, c);
		

		
		c.weightx = 0.5f; c.weighty =0.25f;
		
		c.gridx = 3; c.gridy = 1;
		add = new JButton(" + ");
		pane.add(add, c);
		
		c.gridx = 4; c.gridy = 1;
		subtract = new JButton(" - ");
		pane.add(subtract, c);
		
		c.gridx = 5; c.gridy = 1;
		multiply = new JButton(" * ");
		pane.add(multiply, c);
		
		c.gridx = 4; c.gridy = 2;
		divide = new JButton(" / ");
		pane.add(divide, c);
		
		c.gridx = 3; c.gridy = 2;
		differentiate = new JButton("d/dX");
		pane.add(differentiate, c);
		
		c.gridx = 5; c.gridy = 2;
		integrate = new JButton("∫ dX");
		pane.add(integrate, c);
		
		control = new Controller(add, subtract, divide, multiply, differentiate, integrate, P1, P2, result);
	}
	
	
}
