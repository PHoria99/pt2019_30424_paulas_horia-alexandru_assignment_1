package front;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import behind.Operations;
import behind.Polynomial;

public class Controller implements ActionListener{
	
	private JButton add, subtract, divide, multiply, differentiate, integrate;
	private JTextField P1, P2, result;
	
	Controller(JButton add,JButton subtract,JButton divide,JButton multiply,JButton differentiate,JButton integrate, JTextField P1, JTextField P2, JTextField result)
	{
		this.add=add;
		this.subtract = subtract;
		this.divide = divide;
		this.multiply = multiply;
		this.differentiate = differentiate;
		this.integrate = integrate;
		
		this.P1 = P1;
		this.P2 = P2;
		this.result = result;
		
		
		
		add.addActionListener(this);
		subtract.addActionListener(this);
		divide.addActionListener(this);
		multiply.addActionListener(this);
		differentiate.addActionListener(this);
		integrate.addActionListener(this);
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton b = (JButton) e.getSource();
		
		Polynomial poly1 = new Polynomial();
		Polynomial poly2 = new Polynomial();
		Polynomial res = new Polynomial();
		
		if(b == add) {
			if(!P1.getText().isEmpty() && !P2.getText().isEmpty() && poly1.read(P1.getText())==true && poly2.read(P2.getText())==true) 
				res = Operations.add(poly1, poly2);
			else
			
				JOptionPane.showMessageDialog(null, "Input 2 polynomials!");
			
		}
		
		if(b == subtract){
			if(!P1.getText().isEmpty() && !P2.getText().isEmpty() && poly1.read(P1.getText())==true && poly2.read(P2.getText())==true) 				
				res = Operations.subtract(poly1, poly2);
			
			else
				JOptionPane.showMessageDialog(null, "Input 2 polynomials!");
			
		}
		
		if(b == divide) {
			if(!P1.getText().isEmpty() && !P2.getText().isEmpty() && poly1.read(P1.getText())==true && poly2.read(P2.getText())==true) 
				if(poly2.lead()==null) JOptionPane.showMessageDialog(null,"Division by 0 not allowed!");
				else
					if( poly1.lead().compareTo(poly2.lead()) <= 0 )res = Operations.divide(poly1, poly2);
					else JOptionPane.showMessageDialog(null, "P1 has to have a bigger degree than P2!");
			else
				JOptionPane.showMessageDialog(null, "Input 2 polynomials!");
		}
		
		if(b == multiply) {
			if(!P1.getText().isEmpty() && !P2.getText().isEmpty() && poly1.read(P1.getText())==true && poly2.read(P2.getText())==true) 
				res = Operations.multiply(poly1, poly2);
			else
				JOptionPane.showMessageDialog(null, "Input 2 polynomials!");

			
		}
		
		if(b == differentiate) {
			if(P1.getText().isEmpty() || poly1.read(P1.getText())==false) JOptionPane.showMessageDialog(null, "Input a polynomial in the P1 field!");
			else
			{
				res = Operations.differentiate(poly1);
			}
			
		}
		
		if(b == integrate) {
			if(P1.getText().isEmpty() || poly1.read(P1.getText())==false) JOptionPane.showMessageDialog(null, "Input a polynomial in the P1 field!");
			else
			{
				res = Operations.integrate(poly1);
			}
			
		}
		result.setText(res.print());
			
	}
}
