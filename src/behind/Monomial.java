package behind;

public class Monomial implements Comparable<Monomial>{
	protected int power;
	protected double coefficient;
	
	Monomial()
	{
		this.coefficient = 0.f;
		this.power = 0;
	}
	
	Monomial(double c, int p)
	{
		this.coefficient = c; this.power = p;
	}

	protected void add(Monomial b) {

		if(this.power==b.power)
		this.coefficient = this.coefficient + b.coefficient;
		
	};
	
	protected void subtract(Monomial b) {

		if(this.power == b.power) {
		this.coefficient = this.coefficient - b.coefficient;
		}
		
	};
	
	protected Monomial divide(Monomial b)
	{
		Monomial result = new Monomial(this.coefficient, this.power);
		
		if(this.power >= b.power)
		{
			result.power-= b.power;
			result.coefficient /= b.coefficient;
			return result;
		}
		
		return null;
	}
	
//Integrate monomial, power<-power+1, coefficient<-coefficient/power
	protected void integrate() {
		this.power++;
		float fire = (float)this.coefficient / this.power;		
		double notFire = this.coefficient / this.power;
		
		//Proper approximation of values from float to int
		if(fire-notFire > 0.5f)
		{
			this.coefficient /= this.power; this.coefficient++;
		}
		else
		{
			this.coefficient /= this.power;
		}
	};
	
//Differentiate monomial, coefficient<-coefficient*power, power<-power-1,
	protected void differentiate() {
		this.coefficient *= this.power;
		this.power--;
	}

	public Monomial search(Polynomial p)
	{
		for(Monomial i:p.getPolynomial()) {
			if(i.power == this.power)
				return i;
		}
		
		return null;
	}
	
	public String toString()
	{	
		String mono;
		if(this.coefficient > 0)
			mono = String.format("%.2f", this.coefficient) + "*X^" + this.power;
		else 
			mono = String.format("%.2f", this.coefficient * -1.0f) + "*X^" + this.power;
		return mono;
	}
	
	
	@Override
//Used to compare 2 monomials for sorting and *searching*? purposes
	public int compareTo(Monomial x) {
		if(this.power == x.power)
		{
			return (int)(x.coefficient-this.coefficient);
		}
		else
		{
			return x.power - this.power;
		}
	};
	
}
