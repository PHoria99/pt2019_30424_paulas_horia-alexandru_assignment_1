package behind;

public class Operations {
	
	public static Polynomial add(Polynomial P1, Polynomial P2)
	{
		Polynomial result = new Polynomial(P1);
		
		for(Monomial i:P2.getPolynomial())
		{
			Monomial destination = i.search(result);
			if(destination != null) {
				destination.add(i);
				result.modify(destination);
			}
			else
				result.getPolynomial().add(i);
		}
		result.sort();
		
		return result;
	}
	
	public static Polynomial subtract(Polynomial P1, Polynomial P2)
	{
		Polynomial result = new Polynomial(P1);
		
		for(Monomial i:P2.getPolynomial())
		{
			Monomial destination = i.search(result);
			if(destination != null) {
				destination.subtract(i);
				result.modify(destination);
			}
			else
			{
				i.coefficient = -i.coefficient;
				result.getPolynomial().add(i);
			}
		}
		result.sort();
		
		
		
		return result;
	}
	
	public static Polynomial differentiate(Polynomial P1)
	{
		Polynomial result = new Polynomial(P1);
		
		for(Monomial i:result.getPolynomial())
		{
			i.differentiate();
		}
		
		result.sort();
		return result;
	}
	
	public static Polynomial integrate(Polynomial P1)
	{
		Polynomial result = new Polynomial(P1);
		
		for(Monomial i:result.getPolynomial())
		{
			i.integrate();
		}
		
		result.sort();
		
		
		return result;
	}
	
	public static Polynomial divide(Polynomial P1, Polynomial P2)
	{
		Polynomial result = new Polynomial();

		if(P1.getDegree() >= P2.getDegree()) {
			Polynomial P1_copy = new Polynomial(P1);
			Monomial aux = new Monomial();
			
			while(!P1_copy.isNull() && P1_copy.getDegree() >= P2.getDegree())
			{
				Monomial leadP1 = P1_copy.lead(), leadP2 = P2.lead();
				
				aux = leadP1.divide(leadP2);
				
				if(aux == null) break;
				
				result = Operations.add(result, aux);
				P1_copy = Operations.subtract(P1_copy, Operations.multiply(P2, aux));
			}
		
		}
				
		return result;
	}
	
	public static Polynomial multiply(Polynomial P1, Polynomial P2)
	{
		Polynomial result = new Polynomial();
		
		for(Monomial i:P2.getPolynomial())
		{
			Polynomial aux = new Polynomial(P1);
			aux = Operations.multiply(aux, i);		//Multiply P1 by each member of P2 and then add that to the final result
			
			result = Operations.add(result, aux);
		}
		
		result.sort();
		
		return result;
	}

    //Multiply a Polynomial by a monomial. Auxiliary function for polynomial multiplication
	private static Polynomial multiply(Polynomial P, Monomial x)
	{
		Polynomial result = new Polynomial(P);

		for(Monomial i: result.getPolynomial())
		{
			i.coefficient *= x.coefficient;
			i.power += x.power;
		}
		result.sort();
		
		return result;	
	}
	
	//Add a Monomial to a Polynomial
	private static Polynomial add(Polynomial P, Monomial x)
	{
	Polynomial result = new Polynomial(P);
	
	
	if(!P.getPolynomial().isEmpty()) {
		Monomial destination = x.search(result);
			if(destination != null) {
				destination.add(x);
				result.modify(destination);
			}
			else
				result.getPolynomial().add(x);
			
	}
		else
			result.getPolynomial().add(x);
		
		result.sort();
		
		return result;	
	}
	
}
