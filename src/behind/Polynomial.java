package behind;

import java.util.ArrayList;
import java.util.Collections;

public class Polynomial {
	private ArrayList<Monomial> polynomial;
	
	public Polynomial()
	{
		this.polynomial = new ArrayList<Monomial>();
	}
	
	//Overloaded constructor to copy the argument polynomial into the constructed object
	Polynomial(Polynomial x)
	{
		this.polynomial = new ArrayList<Monomial>();
		
		for(Monomial i : x.getPolynomial())
		{
			this.getPolynomial().add(new Monomial(i.coefficient, i.power));
		}
	}
	
	//Generate the polynomial in string form
	public String print()
	{
		String polyString = "", prevSeparator = "";
		
		for(Monomial i : this.polynomial)
		{
			if(i.coefficient < 0) prevSeparator = " - ";
			
			if(i.coefficient != 0) {
					polyString += prevSeparator + i.toString();
					prevSeparator = " + ";
			}
		}
		
		if(polyString.isEmpty()) polyString+="0";
		
		return polyString;
	}
	
	public boolean read(String polyString)
	{
		if(inputValidation(polyString) == false) return false;
		
		String[] coefficients = polyString.split("\\*X?\\^+[0-9]++(\\s)*");
		String[] powers = polyString.split("(\\+|-)?+(\\s)*+\\d+\\*+X?\\^");
		
		int length = coefficients.length;
		
		for(int i =0; i < length; i++)
		{
			int sign = (coefficients[i].charAt(0) == '-' ) ? -1:1;
			
			coefficients[i]=coefficients[i].replaceAll("\\s","");
			coefficients[i]=coefficients[i].replaceAll("\\+|-", "");
			powers[i]=powers[i].replaceAll("\\s","");
			
			this.polynomial.add(
					new Monomial(sign * Double.parseDouble(coefficients[i]),
							Integer.parseInt(powers[i+1].trim())));
		}
		
		this.sort();
		
		return true;
	}
	
	//Sort the polynomial in descending coefficient order
	protected void sort()
	{
		Collections.sort(this.polynomial);
	}
	
	//Find the element of power x.power, and modify its coefficient to x's
	void modify(Monomial x)
	{
		for(Monomial i : this.polynomial)
		{
			if(i.power == x.power)
				x.coefficient = i.coefficient;
		}
	}
	
	//Getter function to acccess the array list field of the current polynomial
	protected ArrayList<Monomial> getPolynomial() {
		return polynomial;
	}
	
	//Checks if a Polynomial is equal to 0
	protected boolean isNull()
	{
		for(Monomial i: this.polynomial)
		{
			if(i.coefficient != 0) return false;
		}
		return true;
	}
	
	//Finds the leading term of a polynomial
	public Monomial lead()
	{
		for(Monomial i:this.polynomial)
		{
			if(i.coefficient != 0) return i;
		}
		
		return null;
	}

	
	//gets the degree of a polynomial
	protected int getDegree()
	{
		return this.polynomial.get(0).power;
	}
	
	private boolean inputValidation(String s)
	{
		char[] arr = s.toCharArray();
		int length = s.length();
		String verify = new String("0123456789*^X +-");
		String verify2 = new String("0123456789");
		
		for(int i = 0; i < length; i++)
		{ 
			if(verify.indexOf(arr[i]) == -1 && arr[i] != ' ') return false;
			
			if(i>0) {
				if(arr[i]=='^' && arr[i-1]!='X') return false;
			
				if(arr[i]=='*' &&  verify2.indexOf(arr[i-1])==-1) return false;
			
				if(arr[i]=='+' && arr[i-1] != ' ') return false;
			
				if(arr[i]=='-' && arr[i-1] != ' ') return false;
			}
		}
		
		return true;
	}
}
